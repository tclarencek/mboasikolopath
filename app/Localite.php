<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localite extends Model
{
    protected $table="localite";
    protected $primaryKey="localiteid";
    protected $casts = [ 'arrondissementid' => 'integer' ];
}
