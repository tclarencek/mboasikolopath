<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrondissement extends Model
{
    protected $table="arrondissement";
    protected $primaryKey="arrondissementid";
    protected $casts = ['departementid' => 'integer'];
}
