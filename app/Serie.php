<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    protected $table="series";
    protected $primaryKey="seriesid";
    //stop the type cast from string to int
    public $incrementing = false;
    protected $casts=["specialityid"=>"int"];
}
