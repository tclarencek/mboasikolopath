<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table="certificate";
    protected $primaryKey="certificateid";
    // https://stackoverflow.com/questions/38463624/laravel-eloquent-model-id-as-string-return-wrong-value-in
    public $incrementing = false;
}
