<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeboucheSeries extends Model
{
    protected $table="deboucheseries";
    protected $casts=["deboucheid"=>"int"];
}
