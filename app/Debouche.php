<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debouche extends Model
{
    protected $table="debouches";
    protected $primaryKey="deboucheid";
}
