<?php

namespace App\Http\Middleware;

use App\MboaUser;
use Closure;
use Illuminate\Support\Carbon;

class MboaUserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	global $user;

    	if(url()->current() == route('login_url')){
		    return $next($request);
	    }

//    	if api_token not present and not on login page;
    	if(!$request->has('api_token')){
		    return response(json_encode(array(
			    "error"=>"Access Forbidden",
			    "reason"=>"Token Absent as get parameter"
		    )),403);
	    }else{
//    		dd(MboaUser::query()->where('api_token',$request->get('api_token'))
//	                 ->where('token_expiry', '>=', Carbon::now(  )->toDateString())->getQuery()->toSql());
		    $users = MboaUser::query()->where('api_token',$request->get('api_token'))
		                 ->where('token_expiry', '>=', Carbon::now(  )->toDateString())->get(); //timezone in the carbon now 'Africa/Douala'
		    if($users->count()>0){
			    $user = $users->first;
		    }else{
//		    	the token has expired or the token doesn't exist at all
			    return response(json_encode(array(
				    "error"=>"Access Forbidden",
				    "reason"=>"Invalid token"
			    )),403);
		    }
	    }
        return $next($request);
    }
    
}
