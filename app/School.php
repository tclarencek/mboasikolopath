<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table="school";
    protected $primaryKey="schoolid";
    protected $casts=["localiteid"=>"int"];
}
