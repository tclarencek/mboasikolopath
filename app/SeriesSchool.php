<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeriesSchool extends Model
{
    protected $table="seriesschool";
  # https://stackoverflow.com/questions/31527050/laravel-5-controller-sending-json-integer-as-string
	protected $casts = [ 'schoolid' => 'integer' ];

}
