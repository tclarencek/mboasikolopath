<?php

use App\MboaUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|ALTER TABLE `user` ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `password`, ADD `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
function strip($collection){
    $array = $collection->toArray();
    return array_map(function($element){
        unset($element["created_at"]);
        unset($element["updated_at"]);
        return $element;
    },$array);
}

Route::get( '/ping', function ( Request $request ) {

} )->middleware( [ 'user_loggedin' ] );

Route::any( '/login', function ( Request $request ) {
	if ( $request->has( 'username' ) && $request->has( 'password' ) ) {
		$users = MboaUser::query()->where( [
			"username" => $request->get( 'username' ),
			"password" => sha1( $request->get( 'password' ) )
		] )->get();
		if ( $users->count() == 1 ) {
			$user = MboaUser::query()->find( $users->first()->userid );
			if ( $user ) {
				$token              = \Illuminate\Support\Str::random( 60 );
				$user->api_token    = $token;
				$user->token_expiry = \Illuminate\Support\Carbon::now()->addDays( 30 );
				$user->save();
				$user_array = $user->toArray();
				unset( $user_array['password'] );

				return response( json_encode( $user_array ) );
			} else {
				return response( json_encode( array( "error" => "Unexpected error occurred" ) ), 500 );
			}
		} else {
			return response( json_encode( array( "error" => "No such user exists" ) ), 200 );
		}


	}

	return response( json_encode( array( "error" => "Incorrect parameters, expected 'username' and 'password'" ) ), 400 );
//	"Dob", "Username", "UserID", "LocaliteID", "api_token", "token_expiry","password"


} )->middleware( [ 'user_loggedin' ] )->name( 'login_url' );


Route::any( '/signup', function ( Request $request ) {

//	`dob`, `username`, `userid`, `localiteid`, `api_token`, `token_expiry`
	if ( ! empty( \request( 'gender' ) ) &&  ! empty( \request( 'name' ) ) &&  ! empty( \request( 'dob' ) ) && ! empty( \request( 'username' ) ) && $request->has('localiteid') && ! empty( \request( 'password' )  ) ) {
		$token = \Illuminate\Support\Str::random( 60 );
		if ( MboaUser::query()->where( [ 'username' => $request->get( 'username' ) ] )->count() ) {
			return response( json_encode( array( "error" => "Username already exists" ) ), 403 );
		}
        $params             = $request->all();
        unset($params['userid']);
		$params['password'] = sha1( $params['password'] );
		$user               = MboaUser::query()->create( $params );
		$user->api_token    = $token;
		$user->token_expiry = \Illuminate\Support\Carbon::now()->addDays( 30 );
		$user->save();
		$user_array = $user->toArray();
		unset( $user_array['password'] );

		return response( json_encode( $user_array ) );
	} else {
		return response( json_encode( array( "error" => "Incorrect parameters, expected 'username', 'password', 'dob', 'localiteid','name','gender' " ) ), 400 );
	}

} )->name( 'signup_url' );
Route::get( '/pair/debouche-series', function () {
    return strip(\App\DeboucheSeries::query()->get());
} );

Route::group( [ 'middleware' => 'user_loggedin' ], function () {
	// API Core begins

//1. http://APP_DOMAIN/api/pair/certificate-debouches

	/*Route::get( '/pair/certificate-debouches', function () {
		return \App\CertificateDebouche::query()->get(['certificate_certificateid as certificateid','debouche_deboucheid as deboucheid']);
	} );*/
//2. http://APP_DOMAIN/api/pair/section-specialities
	Route::get( '/pair/section-specialities', function () {
		return strip(\App\SectionSpeciality::query()->get(['speciality_specialityid as specialityid','section_sectionid as sectionid']));
	} );
//3. http://APP_DOMAIN/api/pair/series-jobs
	Route::get( '/pair/series-jobs', function () {
		return strip(\App\SeriesJob::query()->get(['job_jobid as jobid','serie_seriesid as seriesid']));
	} );
//4. http://APP_DOMAIN/api/pair/series-schools
	Route::get( '/pair/series-schools', function () {
		return strip(\App\SeriesSchool::query()->get(['school_schoolid as schoolid','serie_seriesid as seriesid']));
	} );
//5. http://APP_DOMAIN/api/pair/series-subjects-taught
	Route::get( '/pair/series-subjects-taught', function () {
		return strip(\App\SeriesSubjectTaught::get(['subject_taught_subjecttaughtid as subjecttaughtid','serie_seriesid as seriesid']));
	} );

//6. http://APP_DOMAIN/api/arrondissements ==> it is out of the middleware


//7. http://APP_DOMAIN/api/certificates
	Route::get( 'certificates', function () {
		return strip(\App\Certificate::all());
	} );

//8. http://APP_DOMAIN/api/debouches
	Route::get( 'debouches', function () {
		return strip(\App\Debouche::all());
	} );

//9. http://APP_DOMAIN/api/departements ==> it is out of the middleware

//10. http://APP_DOMAIN/api/educations
	Route::get( 'educations', function () {
		return strip(\App\Education::all());
	} );

//11. http://APP_DOMAIN/api/jobs
	Route::get( 'jobs', function () {
		return strip(\App\Job::all());
	} );

//12. http://APP_DOMAIN/api/localities ==> it is out of the middleware


//13. http://APP_DOMAIN/api/regions ==> it is out of the middleware


//14. http://APP_DOMAIN/api/schools
	Route::get( 'schools', function () {
		return strip(\App\School::all());
	} );

//15. http://APP_DOMAIN/api/sections
	Route::get( 'sections', function () {
		return strip(\App\Section::all());
	} );

//16. http://APP_DOMAIN/api/series
	Route::get( 'series', function () {
		return strip(\App\Serie::all());
	} );

//17. http://APP_DOMAIN/api/specialities
	Route::get( 'specialities', function () {
		return strip(\App\Speciality::all());
	} );

//18. http://APP_DOMAIN/api/subjects-taught
	Route::get( 'subjects-taught', function () {
		return strip(\App\SubjectTaught::all());
	} );

} );


Route::get( '/add_', function () {
	$tables = DB::select( 'SHOW TABLES' );
	foreach ( $tables as $table ) {
		$t = $table->Tables_in_mboaschoolpath_db;

		try {
			\Illuminate\Support\Facades\DB::statement( 'ALTER TABLE `' . $t . '`  ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , ADD `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;' );
		} catch ( Exception $e ) {
			echo $e->getMessage()."<br>";
		}
	}
} );
Route::get('/tb1',function(){
	$tables = DB::select( 'SHOW TABLES' );
	foreach ( $tables as $table ) {
		$t = $table->Tables_in_mboaschoolpath_db;

		try {
			$columns = DB::getSchemaBuilder()->getColumnListing($t);

			foreach ($columns as $column) {
		$datatype = DB::connection()->getDoctrineColumn($t, $column)->getType()->getName();
				\Illuminate\Support\Facades\DB::statement( 'ALTER TABLE `'.$t.'` CHANGE `'.$column.'` `'.strtolower($column).'` '.$datatype );
			}

		} catch ( Exception $e ) {
			echo $e->getMessage()."<br>";
		}
	}
});
// liberated from the
	Route::get( 'localities', function () {
		return strip(\App\Localite::all());
	} );

	Route::get( 'regions', function () {
		return strip(\App\Region::all());
	} );
	Route::get( 'arrondissements', function () {
		return strip(\App\Arrondissement::all());
	} );
	Route::get( 'departements', function () {
		return strip(\App\Departement::all());
	} );


	Route::get('news',function(){
		return strip(\App\News::all());
	});
